/* 
 * file:        homework.c
 * description: Skeleton code for CS 5600 Homework 2
 *
 * Peter Desnoyers, Northeastern CCIS, 2012
 * $Id: homework.c 530 2012-01-31 19:55:02Z pjd $
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hw2.h"
#include <assert.h>


/********** YOUR CODE STARTS HERE ******************/
enum philo_state 
{
    THINKING,
    EATING,
    WAITING
};

//number of philosophers
#ifdef Q2
#define PCOUNT 4 
#endif

#ifdef Q3
#define PCOUNT 3
#endif

//convenience macros for getting previous and next indices
#define NEXT(i) ((i + 1) % PCOUNT)
#define PREV(i) ((i == 0) ? (PCOUNT - 1) : (i - 1))

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t C[PCOUNT];
pthread_t t[PCOUNT];
int forkQ[PCOUNT];

//fork state
int forks[PCOUNT] = {0};

//philosopher state
enum philo_state philos[PCOUNT] = {0};

//stat counters
int num_philos_thinking = PCOUNT;
void *stat_counter_philos_thinking[PCOUNT + 1] = {NULL};
void *stat_timer_get_forks = NULL;
void *stat_counter_philos_waiting = NULL;


void debugLog(int philosopherIndex, const char *msg)
{
    printf("DEBUG: %f philosopher %d %s\n", timestamp(), philosopherIndex, msg);
}


void updatePhilo(int i, enum philo_state newState)
{
#ifdef Q3
    enum philo_state lastState = philos[i];

    //update thinking counters
    if (newState == THINKING && lastState != THINKING)
    {
        stat_count_decr(stat_counter_philos_thinking[num_philos_thinking]);
        stat_count_incr(stat_counter_philos_thinking[++num_philos_thinking]);
    }
    else if (newState != THINKING && lastState == THINKING) {
        stat_count_decr(stat_counter_philos_thinking[num_philos_thinking]);
        stat_count_incr(stat_counter_philos_thinking[--num_philos_thinking]);
    }

    //update waiting counter
    if (lastState == WAITING && newState != WAITING) {
        stat_count_decr(stat_counter_philos_waiting);
    }
    else if (lastState != WAITING && newState == WAITING) {
        stat_count_incr(stat_counter_philos_waiting);
    }
#endif

    //update philosopher i's state 
    philos[i] = newState;
}


/* get_forks() method - called before a philospher starts eating.
 *                      'i' identifies the philosopher 0..N-1
 */
void get_forks(int i)
{
    #ifdef Q3
    stat_timer_start(stat_timer_get_forks);
    #endif
    
    pthread_mutex_lock(&m);

    debugLog(i, "tries for left fork");
    if (forks[i] == 1)
    {
        while (forks[i] == 1 || !(forkQ[i] == i || forkQ[i] == -1))
        {
            if (forkQ[i] == -1)
            {
                forkQ[i] = i;
            }

            updatePhilo(i, WAITING);
            pthread_cond_wait(&C[i], &m);
        }
        //invariant: philosopher i was the first to attempt to pick up the fork
        assert(forkQ[i] == i || forkQ[i] == -1);
    }
    //invariant: philosopher i's left fork should be available
    assert(forks[i] == 0);

    forkQ[i] = -1;
    forks[i] = 1;
    debugLog(i, "gets left fork");

    debugLog(i, "tries for right fork");
    if (forks[NEXT(i)] == 1)
    {
        while (forks[NEXT(i)] == 1 || !(forkQ[NEXT(i)] == i || forkQ[NEXT(i)] == -1)) 
        {
            if (forkQ[NEXT(i)] == -1)
            {
                forkQ[NEXT(i)] = i;
            }

            updatePhilo(i, WAITING);
            pthread_cond_wait(&C[NEXT(i)], &m);
        }
        //invariant: philosopher i was the first to attempt to pick up the fork
        assert(forkQ[NEXT(i)] == i || forkQ[NEXT(i)] == -1);
    }
    //invariant: philosopher i's right fork should be available
    assert(forks[NEXT(i)] == 0);

    forkQ[NEXT(i)] = -1;
    forks[NEXT(i)] = 1;
    updatePhilo(i, EATING);
    debugLog(i, "gets right fork");

    pthread_mutex_unlock(&m);
    
    #ifdef Q3
    stat_timer_stop(stat_timer_get_forks);
    #endif
}

/* release_forks()  - called when a philospher is done eating.
 *                    'i' identifies the philosopher 0..N-1
 */
void release_forks(int i)
{
    pthread_mutex_lock(&m);
    
    forks[i] = 0;
    forks[NEXT(i)] = 0;
    updatePhilo(i, THINKING);

    pthread_cond_signal(&C[i]);
    pthread_cond_signal(&C[NEXT(i)]);
    
    debugLog(i, "puts down both forks");
    
    pthread_mutex_unlock(&m);
}

/* Threads which call these methods. Note that the pthread create
 * function allows you to pass a single void* pointer value to each
 * thread you create; we actually pass an integer (philosopher number)
 * as that argument instead, using a "cast" to pretend it's a pointer.
 */

/* the customer thread function - create 10 threads, each of which calls
 * this function with its customer number 0..9
 */
void *philosopher_thread(void *context) 
{
    int philosopher_num = (int)context; 

    while (1)
    {
        sleep_exp(5);
        get_forks(philosopher_num);
        sleep_exp(2.5);
        release_forks(philosopher_num);
    }
    
    return 0;
}

void setup()
{
    int i = 0;
    
    //initialize condition variables
    for (i = 0; i < PCOUNT; ++i)
    {
        pthread_cond_init(&C[i], NULL);
        forkQ[i] = -1;
    }

    //start threads
    for (i = 0; i < PCOUNT; ++i)
    {
        pthread_create(&t[i], NULL, philosopher_thread, (void *)i);
    }
}

void q2(void)
{
    setup();
    wait_until_done();
}


/* For question 3 you need to measure the following statistics:
 *
 * 1. The fraction of time there are 3, 2, 1, and 0 philosophers thinking.
 *    (hint - use 4 counters, one for each state, where one of them is
 *    set to 1 and all the others to 0 at any time. The average value
 *    of each counter will be the fraction of time the system is in
 *    the corresponding state)
 * 2. Average time taken by a call to get_forks() (use a timer)
 * 3. Average number of philosophers waiting in the get_forks()
 *    method. (use a counter)
 *
 * The stat_* functions (counter, timer) are described in the PDF. 
 */

void q3(void)
{
    //initialize stat counters and timers
    int i = 0;
    for(; i < PCOUNT + 1; i++) {
        stat_counter_philos_thinking[i] = stat_counter();
    }

    //at first all philosophers are thinking
    stat_count_incr(stat_counter_philos_thinking[num_philos_thinking]);

    stat_timer_get_forks = stat_timer();
    stat_counter_philos_waiting = stat_counter();
    
    //start threads
    setup();
    
    //wait
    wait_until_done();

    //display results
    printf("\n1. Number Thinking | Fraction of Time\n");
    printf("-------------------------------------\n");
    for(i = 0; i < PCOUNT + 1; i++) {
        printf("%18d | %16lf\n", i, stat_count_mean(stat_counter_philos_thinking[i]));
    }

    printf("\n2. Average time taken by a call to get_forks() is ");
    printf("%lf seconds.\n", stat_timer_mean(stat_timer_get_forks));

    printf("\n3. Average number of philosophers waiting in the get_forks() method is ");
    printf("%lf.\n", stat_count_mean(stat_counter_philos_waiting));

    //free counters and timers
    for(i = 0; i < PCOUNT + 1; i++) {
        free(stat_counter_philos_thinking[i]);
    }
    free(stat_timer_get_forks);
    free(stat_counter_philos_waiting);
}
